<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('user_model');
  }

  public function index()
  {
    $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
    $this->form_validation->set_rules('password', 'Password', 'required|trim|min_length[6]', [
      'min_length' => 'Password has must minimum 6 character'
    ]);

    if ($this->form_validation->run() == false) {
      $data = [
        'title_page' => "Login Page"
      ];
      $this->_layout('auth/auth_login', $data);
    } else {
      $this->_login();
    }
  }

  public function register()
  {
    $users = $this->user_model;
    $validation = $this->form_validation;
    $validation->set_rules($users->rules());

    if ($this->form_validation->run() == false) {
      $data = [
        'title_page' => "Register Page"
      ];
      $this->_layout('auth/auth_register', $data);
    } else {
      if ($validation->run()) {
        $users->save();
        $this->session->set_flashdata(
          'message',
          '<div class="alert alert-success" role="alert">
            <small>Success registered!</small>
          </div>'
        );
      }
      redirect(base_url('auth'));
    }
  }

  public function forgot()
  {
    $data = [
      'title_page' => "Forgot Password Page"
    ];
    $this->_layout('auth/auth_forgot', $data);
  }

  public function logout()
  {
    $this->session->unset_userdata('email');
    $this->session->unset_userdata('role_id');
    $this->session->set_flashdata(
      'message',
      '<div class="alert alert-success" role="alert">
        <small>Logout successed!</small>
      </div>'
    );
    redirect(base_url('auth'));
  }

  private function _layout($view, $data)
  {
    $this->load->view('layout/auth/auth_header', $data);
    $this->load->view($view);
    $this->load->view('layout/auth/auth_footer');
  }

  private function _login()
  {
    $email = $this->input->post('email');
    $password = $this->input->post('password');

    $userData = $this->user_model->getUserByEmail($email);
    if ($userData) {
      if ($userData['is_active'] == 1) {
        if (password_verify($password, $userData['password'])) {
          $data = [
            'email' => $userData['email'],
            'role_id' => $userData['role_id']
          ];
          $this->session->set_userdata($data);
          if ($userData['role_id'] == 1) {
            redirect(base_url('admin'));
          } else {
            redirect(base_url('user'));
          }
        } else {
          $this->session->set_flashdata(
            'message',
            '<div class="alert alert-danger" role="alert">
              <small>Your password wrong!</small>
            </div>'
          );
          redirect(base_url('auth'));
        }
      } else {
        $this->session->set_flashdata(
          'message',
          '<div class="alert alert-danger" role="alert">
            <small>Your account has <b>not activated</b>. You can check your email and click the link for activated your account!</small>
          </div>'
        );
        redirect(base_url('auth'));
      }
    } else {
      $this->session->set_flashdata(
        'message',
        '<div class="alert alert-danger" role="alert">
          <small>This email not registered. You can create account first and please login again!</small>
        </div>'
      );
      redirect(base_url('auth'));
    }
  }
}
