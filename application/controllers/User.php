<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('user_model');
  }
  public function index()
  {
    if ($this->session->userdata('email')) {
      $user['user'] = $this->user_model->getUserByEmail($this->session->userdata('email'));
      $title = [
        'title_page' => 'Dashboard Page',
      ];
      $data = [
        'user' => $user['user']
      ];
      $this->_layout('dashboard/user/user_index', $title, $data);
    } else {
      redirect(base_url('auth'));
    }
  }

  private function _layout($view, $title, $data)
  {
    $this->load->view('layout/dashboard/dashboard_header', $title);
    $this->load->view('layout/dashboard/dashboard_topbar', $data);
    $this->load->view('layout/dashboard/dashboard_sidebar');
    $this->load->view($view);
    $this->load->view('layout/dashboard/dashboard_footer');
  }
}
