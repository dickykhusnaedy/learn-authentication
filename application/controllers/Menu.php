<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Menu extends CI_Controller
{
  public function index()
  {
    $user['user'] = $this->user_model->getUserByEmail($this->session->userdata('email'));
    $menu['menu'] = $this->menu_model->getAllMenu();
    $title = [
      'title_page' => 'Menu Management Page',
    ];
    $data = [
      'user' => $user['user'],
      'menu' => $menu['menu']
    ];
    $this->_layout('dashboard/admin/menu/index', $title, $data);
  }

  public function subMenu()
  {
    $user['user'] = $this->user_model->getUserByEmail($this->session->userdata('email'));
    $dataMenu['menu'] = $this->menu_model->getAllMenu();
    $dataSubMenu['subMenu'] = $this->sub_menu_model->getAllSubMenu();
    $title = [
      'title_page' => 'Sub Menu - Menu Management Page',
    ];
    // $this->sub_menu_model->load($id);
    $data = [
      'user' => $user['user'],
      'menu' => $dataMenu['menu'],
      'subMenu' => $dataSubMenu['subMenu'],
    ];
    $this->_layout('dashboard/admin/sub_menu/index', $title, $data);
  }

  public function subChildMenu()
  {
    $user['user'] = $this->user_model->getUserByEmail($this->session->userdata('email'));
    $dataSubMenu['sub_menu'] = $this->sub_menu_model->getSubMenu();
    $dataChildMenu['child_menu'] = $this->sub_child_menu_model->getAllChildMenu();
    $title = [
      'title_page' => 'Child Menu - Menu Management Page',
    ];
    $data = [
      'user' => $user['user'],
      'sub_menu' => $dataSubMenu['sub_menu'],
      'child_menu' => $dataChildMenu['child_menu']
    ];
    $this->_layout('dashboard/admin/sub_child_menu/index', $title, $data);
  }

  public function saveData()
  {
    $menu = $this->menu_model;
    $validation = $this->form_validation;
    $validation->set_rules($menu->rules());

    if ($validation->run()) {
      $menu->save();
      $this->session->set_flashdata(
        'message',
        '<div class="alert alert-success" role="alert">
          <small>Menu successed registered!</small>
        </div>'
      );
    }
    redirect(site_url('menu'));
  }

  public function saveDataSubMenu()
  {
    $subMenu = $this->sub_menu_model;
    $validation = $this->form_validation;
    $validation->set_rules($subMenu->rules());

    if ($validation->run()) {
      $subMenu->saveSubMenu();
      $this->session->set_flashdata(
        'message',
        '<div class="alert alert-success" role="alert">
          <small>Menu successed registered!</small>
        </div>'
      );
    }
    redirect(site_url('menu/sub-menu'));
  }

  public function saveDataSubChildMenu()
  {
    $childMenu = $this->sub_child_menu_model;
    $validation = $this->form_validation;
    $validation->set_rules($childMenu->rules());

    if ($validation->run()) {
      $childMenu->saveSubChildMenu();
      $this->session->set_flashdata(
        'message',
        '<div class="alert alert-success" role="alert">
          <small>Menu successed registered!</small>
        </div>'
      );
    }
    redirect(site_url('menu/child-menu'));
  }

  public function updateData()
  {
    $menu = $this->menu_model;
    $validation = $this->form_validation;
    $validation->set_rules($menu->rules());

    if ($validation->run()) {
      $menu->update();
      $this->session->set_flashdata(
        'message',
        '<div class="alert alert-success" role="alert">
          <small>Menu successed edited!</small>
        </div>'
      );
    }
    redirect(site_url('menu'));
  }

  public function updateDataChildMenu()
  {
    $this->sub_child_menu_model->updateChildMenu();
  }

  public function delete($id)
  {
    $this->menu_model->delete($id);
    $this->session->set_flashdata(
      'message',
      '<div class="alert alert-success" role="alert">
        Success deleted menu!
      </div>'
    );
    redirect(base_url('menu'));
  }

  public function getDataMenu($id)
  {
    $data = $this->menu_model->getMenuById($id);
    echo json_encode($data);
  }

  public function getDataSubMenu($id)
  {
    $data = $this->sub_menu_model->getSubMenuById($id);
    echo json_encode($data);
  }

  public function getDataChildMenu($id)
  {
    $data = $this->sub_child_menu_model->getSubChildMenuById($id);
    echo json_encode($data);
  }

  private function _layout($view, $title, $data)
  {
    $this->load->view('layout/dashboard/dashboard_header', $title);
    $this->load->view('layout/dashboard/dashboard_topbar', $data);
    $this->load->view('layout/dashboard/dashboard_sidebar');
    $this->load->view($view);
    $this->load->view('layout/dashboard/dashboard_footer');
  }
}
