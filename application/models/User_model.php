<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User_model extends CI_Model
{
  private $_table = "user";

  public $first_name;
  public $last_name;
  public $email;
  public $password;
  public $role_id;
  public $is_active;
  public $date_created;
  public $image = "default.jpg";

  public function rules()
  {
    return [
      [
        'field' => 'first_name',
        'label' => 'First Name',
        'rules' => 'required|trim|min_length[3]|xss_clean',
      ],
      [
        'field' => 'email',
        'label' => 'Email',
        'rules' => 'required|trim|valid_email|is_unique[user.email]'
      ],
      [
        'field' => 'password',
        'label' => 'Password',
        'rules' => 'required|trim|min_length[6]'
      ],
      [
        'field' => 'repeat_password',
        'label' => 'Repeat Password',
        'rules' => 'required|trim|matches[password]',
      ],
    ];
  }

  public function getUserByEmail($email)
  {
    return $this->db->get_where($this->_table, ['email' => $email])->row_array();
  }

  public function save()
  {
    $userRegister = $this->input->post();
    $this->first_name = htmlspecialchars($userRegister["first_name"], true);
    $this->last_name = htmlspecialchars($userRegister["last_name"], true);
    $this->email = htmlspecialchars($userRegister["email"], true);
    $this->password = password_hash($userRegister["password"], PASSWORD_DEFAULT);
    $this->role_id = 2;
    $this->is_active = 1;
    $this->date_created = time();
    return $this->db->insert($this->_table, $this);
    var_dump($this);
  }
}
