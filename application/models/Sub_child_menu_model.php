<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Sub_child_menu_model extends CI_Model
{
  private $_table = 'user_sub_child_menu';

  public $id;
  public $sub_menu_id;
  public $child_menu_name;
  public $url;
  public $is_active;

  public function rules()
  {
    return [
      [
        'field' => 'sub_menu_id',
        'lable' => 'Sub Menu',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'child_menu_name',
        'lable' => 'Child Menu Name',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'url',
        'lable' => 'URL',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'is_active',
        'lable' => 'Is Active',
      ]
    ];
  }

  public function getAllChildMenu()
  {
    $query = 'SELECT `user_sub_child_menu`.`*`, `user_sub_menu`.`sub_menu_name`
              FROM `user_sub_child_menu` JOIN `user_sub_menu` ON `user_sub_child_menu`.`sub_menu_id` = `user_sub_menu`.`id`';
    return $this->db->query($query)->result_array();
  }

  public function saveSubChildMenu()
  {
    $data = $this->input->post();
    $this->sub_menu_id = $data['sub_menu_id'];
    $this->child_menu_name = htmlspecialchars($data['child_menu_name'], true);
    $this->url = htmlspecialchars($data['url'], true);
    $this->is_active = $data['is_active'];
    return $this->db->insert($this->_table, $this);
  }

  public function updateChildMenu()
  {
    $data = $this->input->post();
    $this->id = trim($data['id']);
    $this->sub_menu_id = trim($data['sub_menu_id']);
    $this->child_menu_name = trim(htmlspecialchars($data['child_menu_name'], true));
    $this->url = trim(htmlspecialchars($data['url'], true));
    $this->is_active = trim($data['is_active']);
    return $this->db->update($this->_table, $this, ['id' => $this->id]);
  }

  public function getSubChildMenuById($id)
  {
    $query = 'SELECT `user_sub_child_menu`.`*`, `user_sub_menu`.`sub_menu_name`
              FROM `user_sub_child_menu` JOIN `user_sub_menu` ON `user_sub_child_menu`.`sub_menu_id` = `user_sub_menu`.`id`
              WHERE `user_sub_child_menu`.`id` = ' . $id . '';
    return $this->db->query($query)->row_array();
  }
}
