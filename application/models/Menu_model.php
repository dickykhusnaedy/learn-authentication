<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Menu_model extends CI_Model
{
  private $_table = 'user_menu';
  private $_table_sub_menu = 'user_sub_menu';
  private $_table_sub_child_menu = 'user_sub_child_menu';

  public $id;
  public $menu_name;

  public function rules()
  {
    return [
      [
        'field' => 'menu_name',
        'label' => 'Menu Name',
        'rules' => 'required|trim|min_length[3]',
      ],
    ];
  }

  public function save()
  {
    $menuData = $this->input->post();
    $this->menu_name = htmlspecialchars($menuData['menu_name'], true);
    return $this->db->insert($this->_table, $this);
  }

  public function saveSubMenu($data)
  {
    return $this->db->insert($this->_table_sub_menu, $data);
  }

  public function update()
  {
    $menuData = $this->input->post();
    $this->id = $menuData['id_menu'];
    $this->menu_name = htmlspecialchars($menuData['menu_name'], true);
    return $this->db->update($this->_table, $this, ['id' => $this->id]);
  }

  public function getAllMenu()
  {
    return $this->db->get($this->_table)->result_array();
  }

  public function getSubChildMenu()
  {
    $this->db->select('user_sub_child_menu.*');
    $this->db->select('user_sub_menu.sub_menu_name');
    $this->db->from('user_sub_child_menu');
    $this->db->join('user_sub_menu', 'user_sub_child_menu.sub_menu_id = user_sub_menu.id');
    $this->db->where('user_sub_child_menu.is_active=1');
    $data = $this->db->get();
    return $data->result_array();
  }

  public function getMenuById($id)
  {
    return $this->db->get_where($this->_table, ['id' => $id])->row_array();
  }

  public function getSubMenuById($id)
  {
    $this->db->select('user_sub_menu.*');
    $this->db->select('user_menu.menu_name');
    $this->db->from('user_sub_menu');
    $this->db->join('user_menu', 'user_sub_menu.menu_id = user_menu.id');
    $this->db->where('user_sub_menu.id=' . $id);
    $data = $this->db->get();
    return $data->row_array();
  }

  public function delete($id)
  {
    return $this->db->delete($this->_table, ['id' => $id]);
  }
}
