<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Sub_menu_model extends CI_Model
{
  private $_table = 'user_sub_menu';

  public $id;
  public $menu_id;
  public $sub_menu_name;
  public $url;
  public $is_active;
  public $is_child_menu;

  public function rules()
  {
    return [
      [
        'field' => 'menu_id',
        'lable' => 'Main Menu',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'sub_menu_name',
        'lable' => 'Sub Menu Name',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'url',
        'lable' => 'URL',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'is_active',
        'lable' => 'Is Active',
      ],
      [
        'field' => 'is_child_menu',
        'lable' => 'Is Child Menu',
      ],
    ];
  }

  public function getAllSubMenu()
  {
    $query = 'SELECT `user_sub_menu`.`*`, `user_menu`.`menu_name`
              FROM `user_sub_menu` JOIN `user_menu` ON `user_sub_menu`.`menu_id` = `user_menu`.`id`';
    return $this->db->query($query)->result_array();
  }

  public function getSubMenu()
  {
    $query = 'SELECT `user_sub_menu`.`*`, `user_menu`.`menu_name`
              FROM `user_sub_menu` JOIN `user_menu` ON `user_sub_menu`.`menu_id` = `user_menu`.`id`
              WHERE `user_sub_menu`.`is_active` = 1';
    return $this->db->query($query)->result_array();
  }

  public function saveSubMenu()
  {
    $data = $this->input->post();
    $this->menu_id = $data['menu_id'];
    $this->sub_menu_name = htmlspecialchars($data['sub_menu_name'], true);
    $this->url = htmlspecialchars($data['url'], true);
    $this->is_active = $data['is_active'];
    $this->is_child_menu = $data['is_child_menu'];
    return $this->db->insert($this->_table, $this);
  }

  public function getSubMenuById($id)
  {
    $query = 'SELECT `user_sub_menu`.`*`, `user_menu`.`menu_name`
              FROM `user_sub_menu` JOIN `user_menu` ON `user_sub_menu`.`menu_id` = `user_menu`.`id`
              WHERE `user_sub_menu`.`id` = ' . $id . '';
    return $this->db->query($query)->row_array();
  }
}
