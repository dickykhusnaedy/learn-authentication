<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title><?= $title_page ?></title>
  <!-- plugins:css -->
  <!-- plugin css for this page -->
  <link rel="stylesheet" href="<?= base_url('assets') ?>/vendors/iconfonts/mdi/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css" />
  <link rel="stylesheet" href="<?= base_url('assets') ?>/vendors/css/vendor.bundle.base.css">
  <link rel="stylesheet" href="<?= base_url('assets') ?>/vendors/css/vendor.bundle.addons.css">
  <link rel="stylesheet" href="<?= base_url('assets') ?>/css/plugin_css/sweetalert2.min.css" />
  <link rel="stylesheet" href="<?= base_url('assets') ?>/css/plugin_css/bootstrap-select.min.css" />
  <!-- endinject -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="<?= base_url('assets') ?>/css/shared/style.css">
  <!-- endinject -->
  <!-- Layout styles -->
  <link rel="stylesheet" href="<?= base_url('assets') ?>/css/demo_1/style.css">
  <link rel="shortcut icon" href="<?= base_url('assets') ?>/images/favicon.ico" />
  <!-- End Layout styles -->

  <!-- plugins:js -->
  <script src="<?= base_url('assets') ?>/vendors/js/vendor.bundle.base.js"></script>
  <script src="<?= base_url('assets') ?>/js/plugin_js/sweetalert2.all.min.js"></script>
  <script src="<?= base_url('assets') ?>/js/plugin_js/bootstrap-select.min.js"></script>
  <!-- endinject -->
</head>

<body>

  <div class="container-scroller">