<!-- partial:partials/_sidebar.html -->
<nav class="sidebar sidebar-offcanvas" id="sidebar">
  <?php
  $role_id = $this->session->userdata('role_id');
  $queryMenu = "SELECT `user_menu`.`id`, `user_menu`.`menu_name`
                FROM `user_menu` JOIN `user_access_menu` 
                ON `user_menu`.`id` = `user_access_menu`.`menu_id`
                WHERE `user_access_menu`.`role_id` = $role_id
              ";
  $queryResultMenu = $this->db->query($queryMenu)->result_array();
  ?>
  <?php foreach ($queryResultMenu as $itemMenu) : $menuId = $itemMenu['id']; ?>
    <ul class="nav mt-3">
      <li class="nav-item nav-category"><?= $itemMenu['menu_name'] ?></li>
      <?php
      $querySubMenu = "SELECT `user_sub_menu`.*
                      FROM `user_sub_menu` JOIN `user_menu`
                      ON `user_sub_menu`.`menu_id` = `user_menu`.`id`
                      WHERE `user_sub_menu`.`menu_id` = $menuId 
                      AND `user_sub_menu`.`is_active` = 1";
      $queryResultSubMenu = $this->db->query($querySubMenu)->result_array();
      ?>
      <?php foreach ($queryResultSubMenu as $itemSubMenu) : $subMenuId = $itemSubMenu['id'] ?>
        <?php if ($itemSubMenu['is_child_menu']) : ?>
          <?php
          $querySubChildMenu = "SELECT `user_sub_child_menu`.*
                      FROM `user_sub_child_menu` JOIN `user_sub_menu`
                      ON `user_sub_child_menu`.`sub_menu_id` = `user_sub_menu`.`id`
                      WHERE `user_sub_child_menu`.`sub_menu_id` = $subMenuId
                      AND `user_sub_menu`.`is_child_menu` = 1
                      AND `user_sub_child_menu`.`is_active` = 1";
          $queryResultSubChildMenu = $this->db->query($querySubChildMenu)->result_array();
          // print_r($queryResultSubChildMenu);
          ?>
          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
              <i class="menu-icon typcn typcn-coffee"></i>
              <span class="menu-title"><?= $itemSubMenu['sub_menu_name'] ?></span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="ui-basic">
              <ul class="nav flex-column sub-menu">
                <?php foreach ($queryResultSubChildMenu as $itemWithChild) : ?>
                  <li class="nav-item">
                    <a class="nav-link" href="<?= site_url($itemWithChild['url']) ?>"><?= $itemWithChild['child_menu_name'] ?></a>
                  </li>
                <?php endforeach; ?>
              </ul>
            </div>
          </li>
        <?php else : ?>
          <li class="nav-item">
            <a class="nav-link" href="<?= base_url($itemSubMenu['url']) ?>">
              <i class="menu-icon typcn typcn-document-text"></i>
              <span class="menu-title"><?= $itemSubMenu['sub_menu_name'] ?></span>
            </a>
          </li>
        <?php endif; ?>
      <?php endforeach; ?>
    </ul>
  <?php endforeach; ?>
</nav>
<!-- partial -->
<div class="main-panel">