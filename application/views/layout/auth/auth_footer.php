<!-- Plugin js for this page-->
<!-- plugins:js -->
<script src="<?= base_url('assets') ?>/vendors/js/vendor.bundle.base.js"></script>
<script src="<?= base_url('assets') ?>/vendors/js/vendor.bundle.addons.js"></script>
<!-- endinject -->
<!-- End plugin js for this page-->
<!-- inject:js -->
<script src="<?= base_url('assets') ?>/js/shared/off-canvas.js"></script>
<script src="<?= base_url('assets') ?>/js/shared/misc.js"></script>
<!-- endinject -->
<!-- Custom js for this page-->
<script src="<?= base_url('assets') ?>/js/demo_1/dashboard.js"></script>
</body>

</html>