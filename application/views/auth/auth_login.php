<div class="container-scroller">
  <div class="container-fluid page-body-wrapper full-page-wrapper">
    <div class="content-wrapper d-flex align-items-center auth auth-bg-1 theme-one">
      <div class="row w-100">
        <div class="col-lg-4 mx-auto">
          <div class="auto-form-wrapper">
            <div class="text-center">
              <h4 class="font-weight-semibold">Login Page</h4>
              <p class="mb-4" style="color:grey;font-size: 12px">Please login to your account before access the system</p>
            </div>
            <?= $this->session->flashdata('message') ?>
            <form action="<?= base_url('auth') ?>" method="POST">
              <div class="form-group">
                <label class="label">Email address</label>
                <div class="input-group">
                  <input type="text" class="form-control" placeholder="Email address" id="email" name="email" value="<?= set_value('email') ?>">
                  <div class="input-group-append">
                    <span class="input-group-text">
                      <i class="mdi mdi-email"></i>
                    </span>
                  </div>
                </div>
                <?= form_error('email', '<div class="invalid-feedback">', '</div>') ?>
              </div>
              <div class="form-group">
                <label class="label">Password</label>
                <div class="input-group">
                  <input type="password" class="form-control" placeholder="*********" id="password" name="password">
                  <div class="input-group-append">
                    <span class="input-group-text">
                      <i class="mdi mdi-onepassword"></i>
                    </span>
                  </div>
                </div>
                <?= form_error('password', '<div class="invalid-feedback">', '</div>') ?>
              </div>
              <div class="form-group">
                <button class="btn btn-primary submit-btn btn-block" type="submit">Login</button>
              </div>
              <div class="form-group d-flex justify-content-end">
                <a href="<?= base_url('auth/forgot') ?>" class="text-black" style="font-size: 11px;">Forgot Password?</a>
              </div>
              <hr>
              <div class="text-block text-center my-3">
                <span class="text-small font-weight-semibold">Not a member ?</span>
                <a href="<?= base_url('auth/register') ?>" class="text-black text-small">Create new account</a>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    <!-- content-wrapper ends -->
  </div>
  <!-- page-body-wrapper ends -->
</div>
<!-- container-scroller -->