<div class="container-scroller">
  <div class="container-fluid page-body-wrapper full-page-wrapper">
    <div class="content-wrapper d-flex align-items-center auth auth-bg-1 theme-one">
      <div class="row w-100">
        <div class="col-lg-4 mx-auto">
          <div class="auto-form-wrapper">
            <div class="text-center">
              <h4 class="font-weight-semibold">Register Page</h4>
              <p class="mb-4" style="color:grey;font-size: 12px">Please completed this form for register to system</p>
            </div>
            <form action="<?= base_url('auth/register') ?>" method="POST">
              <div class="row">
                <div class="form-group col-md-6">
                  <label class="label">First Name</label>
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="First name" id="first_name" name="first_name" value="<?= set_value('first_name') ?>">
                    <div class="input-group-append">
                      <span class="input-group-text">
                        <i class="mdi mdi-face-profile"></i>
                      </span>
                    </div>
                  </div>
                  <?= form_error('first_name', '<div class="invalid-feedback">', '</div>') ?>
                </div>
                <div class="form-group col-md-6">
                  <label class="label">Last Name</label>
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Last name" id="last_name" name="last_name" value="<?= set_value('last_name') ?>">
                    <div class="input-group-append">
                      <span class="input-group-text">
                        <i class="mdi mdi-face-profile"></i>
                      </span>
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label class="label">Email</label>
                <div class="input-group">
                  <input type="text" class="form-control" placeholder="Email address" id="email" name="email" value="<?= set_value('email') ?>">
                  <div class="input-group-append">
                    <span class="input-group-text">
                      <i class="mdi mdi-email"></i>
                    </span>
                  </div>
                </div>
                <?= form_error('email', '<div class="invalid-feedback">', '</div>') ?>
              </div>
              <div class="row">
                <div class="form-group col-md-6">
                  <label class="label">Password</label>
                  <div class="input-group">
                    <input type="password" class="form-control" placeholder="*********" id="password" name="password">
                    <div class="input-group-append">
                      <span class="input-group-text">
                        <i class="mdi mdi-onepassword"></i>
                      </span>
                    </div>
                  </div>
                  <?= form_error('password', '<div class="invalid-feedback">', '</div>') ?>
                </div>
                <div class="form-group col-md-6">
                  <label class="label">Repeat Password</label>
                  <div class="input-group">
                    <input type="password" class="form-control" placeholder="*********" id="repeat_password" name="repeat_password">
                    <div class="input-group-append">
                      <span class="input-group-text">
                        <i class="mdi mdi-onepassword"></i>
                      </span>
                    </div>
                  </div>
                  <?= form_error('repeat_password', '<div class="invalid-feedback">', '</div>') ?>
                </div>
              </div>
              <div class="form-group">
                <button class="btn btn-primary submit-btn btn-block" type="submit">Register</button>
              </div>
              <hr>
              <div class="text-block text-center my-3">
                <span class="text-small font-weight-semibold">Already have and account ?</span>
                <a href="<?= base_url('auth') ?>" class="text-black text-small">Login</a>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    <!-- content-wrapper ends -->
  </div>
  <!-- page-body-wrapper ends -->
</div>
<!-- container-scroller -->