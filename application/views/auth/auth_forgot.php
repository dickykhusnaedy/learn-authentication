<div class="container-scroller">
  <div class="container-fluid page-body-wrapper full-page-wrapper">
    <div class="content-wrapper d-flex align-items-center auth auth-bg-1 theme-one">
      <div class="row w-100">
        <div class="col-lg-4 mx-auto">
          <div class="auto-form-wrapper">
            <div class="text-center">
              <h4 class="font-weight-semibold">Forgot Password Page</h4>
              <p class="mb-4" style="color:grey;font-size: 12px">Please entered your email</p>
            </div>
            <form action="#">
              <div class="form-group">
                <label class="label">Email address</label>
                <div class="input-group">
                  <input type="text" class="form-control" placeholder="Email address" id="email" name="email">
                  <div class="input-group-append">
                    <span class="input-group-text">
                      <i class="mdi mdi-email"></i>
                    </span>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <button class="btn btn-primary submit-btn btn-block" type="submit">Send Link</button>
              </div>
              <hr>
              <div class="text-block text-center my-3">
                <span class="text-small font-weight-semibold">Remember your password ?</span>
                <a href="<?= base_url('auth') ?>" class="text-black text-small">Login</a>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    <!-- content-wrapper ends -->
  </div>
  <!-- page-body-wrapper ends -->
</div>
<!-- container-scroller -->