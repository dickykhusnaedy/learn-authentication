<div class="content-wrapper">
  <!-- Page Title Header Starts-->
  <div class="row page-title-header">
    <div class="col-12">
      <div class="page-header">
        <h4 class="page-title">Menu</h4>
      </div>
    </div>
  </div>
  <!-- Page Title Header Ends-->
  <div class="row">
    <div class="col-md-8 grid-margin">
      <div class="card">
        <div class="card-body">
          <!-- Button trigger modal -->
          <button type="button" class="btn btn-sm btn-primary mb-4" data-bs-toggle="modal" data-bs-target="#addMenu">
            Add New Menu
          </button>
          <?= $this->session->flashdata('message') ?>
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
              <thead>
                <tr>
                  <th style="width: 10%;text-align:center">#</th>
                  <th style="width: 80%;">Menu Name</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <?php $no = 1;
                foreach ($menu as $item) : ?>
                  <tr>
                    <td style="text-align: center;"><?= $no++ ?></td>
                    <td><?= $item['menu_name'] ?></td>
                    <td>
                      <a href="#" data-id="<?= $item['id'] ?>" data-bs-target="#editMenu" data-bs-toggle="modal" class="btn btn-sm btn-primary btnEdit pr-1 pl-2"><i class="fas fa-pencil-alt"></i></a>
                      <a href="#" data-id="<?= $item['id'] ?>" class="btn btn-sm btn-danger deleteMenu pr-1 pl-2"><i class="fas fa-trash"></i></a>
                    </td>
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Add Menu Modal -->
<div class="modal fade" id="addMenu" tabindex="-1" aria-labelledby="addMenuModal" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title font-weight-bold" id="addMenuModal">Add Menu</h5>
        <button type="button" class="close pr-4 pt-4" data-bs-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?= base_url('menu/saveData') ?>" method="post">
        <div class="modal-body">
          <div class="mb-3">
            <label class="form-label text-small">Menu Name</label>
            <input type="text" class="form-control" id="menu_name" name="menu_name">
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save</button>
        </div>
      </form>
    </div>
  </div>
</div>

<!-- Edit Menu Modal -->
<div class="modal fade" id="editMenu" tabindex="-1" aria-labelledby="editMenuModal" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title font-weight-bold" id="editMenuModal">Edit Menu</h5>
        <button type="button" class="close pr-4 pt-4" data-bs-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?= base_url('menu/updateData') ?>" method="post">
        <div class="modal-body">
          <div class="mb-3">
            <input type="hidden" class="form-control" id="edit_id_menu" name="id_menu">
            <label class="form-label text-small">Menu Name</label>
            <input type="text" class="form-control" id="edit_menu_name" name="menu_name">
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save</button>
        </div>
      </form>
    </div>
  </div>
</div>

<script>
  $('.deleteMenu').on('click', function() {
    const id = $(this).data('id');
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        window.location.href = "<?= base_url('menu/delete/') ?>" + id;
        Swal.fire(
          'Deleted!',
          'Your file has been deleted.',
          'success'
        )
      } else {
        Swal.fire(
          'Cancelled!',
          'Your imaginary file is safe :)',
          'error'
        )
      }
    })
  })

  $('.btnEdit').on('click', function() {
    const id = $(this).data('id');
    $.ajax({
      url: "<?= base_url('menu/getDataMenu/') ?>" + id,
      type: 'post',
      dataType: 'JSON',
      success: function(data) {
        $('#edit_id_menu').val(data.id);
        $('#edit_menu_name').val(data.menu_name);
      }
    })
  })
</script>