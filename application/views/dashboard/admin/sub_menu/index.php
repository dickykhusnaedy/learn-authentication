<div class="content-wrapper">
  <!-- Page Title Header Starts-->
  <div class="row page-title-header">
    <div class="col-12">
      <div class="page-header">
        <h4 class="page-title">Sub Menu</h4>
      </div>
    </div>
  </div>
  <!-- Page Title Header Ends-->
  <div class="row">
    <div class="col-lg-10 grid-margin">
      <div class="card">
        <div class="card-body">
          <!-- Button trigger modal -->
          <button type="button" class="btn btn-sm btn-primary mb-4" data-bs-toggle="modal" data-bs-target="#addSubMenu">
            Add Sub Menu
          </button>
          <?= $this->session->flashdata('message') ?>
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
              <thead>
                <tr>
                  <th style="width: 8%;text-align: center">#</th>
                  <th style="width: 20%;">Main Menu</th>
                  <th style="width: 30%;">Sub Menu Name</th>
                  <th style="width: 30%;">URL</th>
                  <th style="width: 10%;">Has Child Menu?</th>
                  <th style="width: 10%;">Active?</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <?php $no = 1;
                foreach ($subMenu as $item) : ?>
                  <tr>
                    <td style="text-align: center;"><?= $no++ ?></td>
                    <td><?= $item['menu_name'] ?></td>
                    <td><?= $item['sub_menu_name'] ?></td>
                    <td class="pl-0">
                      <pre><?= $item['url'] ?></pre>
                    </td>
                    <td style="text-align: center;">
                      <?= $item['is_child_menu'] == 1 ? '<span class="badge badge-success">Yes</span>' : '-' ?>
                    </td>
                    <td style="text-align: center;">
                      <?= $item['is_active'] == 1 ? '<span class="badge badge-success">Active</span>' : '-' ?>
                    </td>
                    <td>
                      <a href="#" data-id="<?= $item['id'] ?>" data-bs-target="#editSubMenu" data-bs-toggle="modal" class="btn btn-sm btn-primary btnEdit pr-1 pl-2"><i class="fas fa-pencil-alt"></i></a>
                      <a href="#" data-id="<?= $item['id'] ?>" class="btn btn-sm btn-danger deleteMenu pr-1 pl-2"><i class="fas fa-trash"></i></a>
                    </td>
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Add Menu Modal -->
<div class="modal fade" id="addSubMenu" tabindex="-1" aria-labelledby="addSubMenuModal" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title font-weight-bold" id="addSubMenuModal">Add Sub Menu</h5>
        <button type="button" class="close pr-4 pt-4" data-bs-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?= base_url('menu/saveDataSubMenu') ?>" method="post">
        <div class="modal-body">
          <div class="form-group">
            <label for="menu_id">Main Menu</label>
            <select class="select form-control" data-live-search="true" data-style="btn-primary" data-size="6" name="menu_id" id="menu_id">
              <?php foreach ($menu as $item) : ?>
                <option value="<?= $item['id'] ?>" style="text-transform: uppercase"><?= $item['menu_name'] ?></option>
              <?php endforeach; ?>
            </select>
          </div>
          <div class="mb-3">
            <label class="form-label text-small">Sub Menu Name</label>
            <input type="text" class="form-control" id="sub_menu_name" name="sub_menu_name">
          </div>
          <label class="form-label text-small">URL</label>
          <div class="input-group mb-3">
            <div class="input-group-prepend">
              <span class="input-group-text" id="url"><?= base_url() ?></span>
            </div>
            <input type="text" class="form-control" name="url" id="url" aria-describedby="url">
          </div>
          <div class="row d-flex justify-content-center align-items-center">
            <div class="col">
              <label class="form-label text-small">Has Child Menu?</label>
            </div>
            <div class="col">
              <div class="form-group form-inline mt-0 text-small">
                <div class="form-radio mr-3">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="is_child_menu" id="optionsYes" value="1">Yes</label>
                </div>
                <div class="form-radio">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="is_child_menu" id="optionsNo" value="" checked>No</label>
                </div>
              </div>
            </div>
          </div>
          <div class="form-group" style="margin-top: -1rem;">
            <div class="form-check form-check-flat">
              <label class="form-check-label">
                <input type="checkbox" class="form-check-input" value="1" name="is_active" id="is_active" checked> Is Active? </label>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Save</button>
            </div>
          </div>
      </form>
    </div>
  </div>
</div>
</div>

<!-- Edit Menu Modal -->
<div class="modal fade" id="editSubMenu" tabindex="-1" aria-labelledby="editSubMenuModal" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title font-weight-bold" id="editSubMenuModal">Edit Menu</h5>
        <button type="button" class="close pr-4 pt-4" data-bs-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?= base_url('menu/saveDataSubMenu') ?>" method="post">
        <div class="modal-body">
          <div class="form-group">
            <label for="menu_id">Main Menu</label>
            <input type="text" class="form-control" id="edit_menu" name="edit_menu" readonly style="text-transform: uppercase;">
          </div>
          <div class="mb-3">
            <label class="form-label text-small">Sub Menu Name</label>
            <input type="text" class="form-control" id="edit_sub_menu_name" name="sub_menu_name">
          </div>
          <label class="form-label text-small">URL</label>
          <div class="input-group mb-3">
            <div class="input-group-prepend">
              <span class="input-group-text" id="url"><?= base_url() ?></span>
            </div>
            <input type="text" class="form-control" id="edit_url" name="url">
          </div>
          <div class="row d-flex justify-content-center align-items-center">
            <div class="col">
              <label class="form-label text-small">Has Child Menu?</label>
            </div>
            <div class="col">
              <div class="form-group form-inline mt-0 text-small">
                <div class="form-radio mr-3">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="is_child_menu" id="edit_options" value="1">Yes</label>
                </div>
                <div class="form-radio">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="is_child_menu" id="edit_option" value="">No</label>
                </div>
              </div>
            </div>
          </div>
          <div class="form-group" style="margin-top: -1rem;">
            <div class="form-check form-check-flat">
              <label class="form-check-label">
                <input type="checkbox" class="form-check-input" value="1" name="is_active" id="edit_is_active"> Is Active? </label>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save</button>
        </div>
      </form>
    </div>
  </div>
</div>

<script>
  $(".select").selectpicker();

  $('.btnEdit').on('click', function() {
    const id = $(this).data('id');
    $.ajax({
      url: "<?= base_url('menu/getDataSubMenu/') ?>" + id,
      type: 'post',
      dataType: 'JSON',
      success: function(data) {
        $('#edit_menu').val(data.menu_name);
        $('#edit_sub_menu_name').val(data.sub_menu_name);
        $('#edit_url').val(data.url);
        if (data.is_child_menu == 1) {
          $('#edit_options').prop('checked', true);
        } else {
          $('#edit_option').prop('checked', true);
        }
        if (data.is_active == 1) {
          $('#edit_is_active').prop('checked', true);
        } else {
          $('#edit_is_active').prop('checked', false);
        }
      }
    })
  })
</script>