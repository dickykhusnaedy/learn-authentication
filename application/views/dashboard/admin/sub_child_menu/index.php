<div class="content-wrapper">
  <!-- Page Title Header Starts-->
  <div class="row page-title-header">
    <div class="col-12">
      <div class="page-header">
        <h4 class="page-title">Sub Child Menu</h4>
      </div>
    </div>
  </div>
  <!-- Page Title Header Ends-->
  <!-- Page Title Header Ends-->
  <div class="row">
    <div class="col-lg-8 grid-margin">
      <div class="card">
        <div class="card-body">
          <!-- Button trigger modal -->
          <button type="button" class="btn btn-sm btn-primary mb-4" data-bs-toggle="modal" data-bs-target="#addSubChildMenu">
            Add Child Menu
          </button>
          <?= $this->session->flashdata('message') ?>
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
              <thead>
                <tr>
                  <th style="width: 8%;text-align: center">#</th>
                  <th style="width: 30%;">Sub Menu Name</th>
                  <th style="width: 40%;">Child Menu Name</th>
                  <th style="width: 30%;">URL</th>
                  <th style="width: 30%;">Active?</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <?php $no = 1;
                foreach ($child_menu as $item) : ?>
                  <tr>
                    <td style="text-align: center;"><?= $no++ ?></td>
                    <td><?= $item['sub_menu_name'] ?></td>
                    <td><?= $item['child_menu_name'] ?></td>
                    <td class="pl-0">
                      <pre><?= $item['url'] ?></pre>
                    </td>
                    <td style="text-align: center;">
                      <?= $item['is_active'] == 'Y' ? '<span class="badge badge-success">Active</span>' : '-' ?>
                    </td>
                    <td>
                      <a href="#" data-id="<?= $item['id'] ?>" data-bs-target="#editSubChildMenu" data-bs-toggle="modal" class="btn btn-sm btn-primary btnEdit pr-1 pl-2"><i class="fas fa-pencil-alt"></i></a>
                      <a href="#" data-id="<?= $item['id'] ?>" class="btn btn-sm btn-danger deleteMenu pr-1 pl-2"><i class="fas fa-trash"></i></a>
                    </td>
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Add child menu modal -->
<div class="modal fade" id="addSubChildMenu" tabindex="-1" aria-labelledby="addSubChildMenuModal" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title font-weight-bold" id="addSubChildMenuModal">Add Sub Child Menu</h5>
        <button type="button" class="close pr-4 pt-4" data-bs-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?= base_url('menu/saveDataSubChildMenu') ?>" method="post">
        <div class="modal-body">
          <div class="form-group">
            <label for="exampleInputEmail1">Sub Menu</label>
            <select class="select form-control" data-live-search="true" data-style="btn-primary" data-size="6" name="sub_menu_id" id="sub_menu_id">
              <?php foreach ($sub_menu as $item) : ?>
                <option value="<?= $item['id'] ?>" style="text-transform: uppercase"><?= $item['sub_menu_name'] ?></option>
              <?php endforeach; ?>
            </select>
          </div>
          <div class="mb-3">
            <label class="form-label text-small">Child Menu Name</label>
            <input type="text" class="form-control" id="child_menu_name" name="child_menu_name">
          </div>
          <label class="form-label text-small">URL</label>
          <div class="input-group mb-3">
            <div class="input-group-prepend">
              <span class="input-group-text" id="url"><?= base_url() ?></span>
            </div>
            <input type="text" class="form-control" id="url" name="url">
          </div>
          <div class="form-group">
            <div class="form-check form-check-flat">
              <label class="form-check-label">
                <input type="checkbox" class="form-check-input" value="1" name="is_active" id="is_active"> Is Active? </label>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save</button>
        </div>
      </form>
    </div>
  </div>
</div>

<!-- Edit child menu modal -->
<div class="modal fade" id="editSubChildMenu" tabindex="-1" aria-labelledby="editSubChildMenuModal" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title font-weight-bold" id="editSubChildMenuModal">Edit Child Menu</h5>
        <button type="button" class="close pr-4 pt-4" data-bs-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form>
        <div class="modal-body">
          <input type="hidden" class="form-control" id="id_child_menu" name="id">
          <input type="hidden" class="form-control" id="sub_menu_id" name="sub_menu_id">
          <div class="form-group">
            <label for="exampleInputEmail1">Sub Menu</label>
            <input type="text" class="form-control" id="edit_sub_menu" name="sub_menu_id" readonly style="text-transform: uppercase;">
          </div>
          <div class="mb-3">
            <label class="form-label text-small">Child Menu Name</label>
            <input type="text" class="form-control" id="edit_child_menu_name" name="child_menu_name">
          </div>
          <label class="form-label text-small">URL</label>
          <div class="input-group mb-3">
            <div class="input-group-prepend">
              <span class="input-group-text" id="url"><?= base_url() ?></span>
            </div>
            <input type="text" class="form-control" id="edit_url" name="url">
          </div>
          <div class="form-group">
            <div class="form-check form-check-flat">
              <label class="form-check-label">
                <input type="checkbox" class="form-check-input" value="Y" name="is_active" id="edit_is_active"> Is Active? </label>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary" id="updateData">Update</button>
        </div>
      </form>
    </div>
  </div>
</div>

<script>
  $(".select").selectpicker();

  $('.btnEdit').on('click', function() {
    var id = $(this).data('id');
    $.ajax({
      url: "<?= base_url('menu/getDataChildMenu/') ?>" + id,
      type: 'post',
      dataType: 'JSON',
      success: function(data) {
        $('#id_child_menu').val(data.id);
        $('#sub_menu_id').val(data.sub_menu_id);
        $('#edit_sub_menu').val(data.sub_menu_name);
        $('#edit_child_menu_name').val(data.child_menu_name);
        $('#edit_url').val(data.url);
        if (data.is_active == 'Y') {
          $('#edit_is_active').prop('checked', true);
        } else {
          $('#edit_is_active').prop('checked', false);
        }
      }
    })
  })

  $('#updateData').on('click', function() {
    var isActive = '';
    var id = $('#id_child_menu').val();
    var subMenu = $('#sub_menu_id').val();
    var childMenuName = $('#edit_child_menu_name').val();
    var url = $('#edit_url').val();
    var checked = $('input[name="is_active"]:checked').length > 0;
    checked ? isActive = 'Y' : isActive = 'N';
    // pass object in post
    var dataChildMenu = {
      'id': id,
      'sub_menu_id': subMenu,
      'child_menu_name': childMenuName,
      'url': url,
      'is_active': isActive
    };
    console.log(dataChildMenu)
    console.log(isActive);
    $.ajax({
      type: 'POST',
      url: '<?= site_url('menu/updateDataChildMenu') ?>',
      data: dataChildMenu,
      dataType: 'json'
    });
    window.location.href = '<?= site_url('menu/child-menu') ?>'
  })
</script>